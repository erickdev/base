package com.backend.projeto.config;

import com.backend.projeto.entity.Role;
import com.backend.projeto.entity.User;
import com.backend.projeto.repository.RoleRepository;
import com.backend.projeto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializr implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public RoleRepository roleRepository;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            this.createUsers("Erick", "erick@gmail.com",
                    passwordEncoder().encode("12345"),
                    "ROLE_ALUNO");
            this.createUsers("admin", "admin@gmail.com",
                    passwordEncoder().encode("12345"),
                    "ROLE_ADMIN");
        }
    }

    public void createUsers(String name, String email, String password, String role) {
        Role roleObject = new Role();
        roleObject.setName(role);

        this.roleRepository.save(roleObject);

        User user = new User(name, email, password, Arrays.asList(roleObject));
        userRepository.save(user);
    }
}
